let taskApp = () => {
    return {
        newTask: '',
        tasks: [],

        submit() {
            this.tasks.push({ body: this.newTask, completed: false });
            
            this.newTask = '';
        }
    }
}